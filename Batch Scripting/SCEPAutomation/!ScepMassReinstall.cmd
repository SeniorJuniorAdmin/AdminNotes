@setlocal enabledelayedexpansion 
FOR /F "skip=1 delims=" %%a IN (%~dp0\Report.csv) DO 
( 
	FOR /F %%f IN ('dir /b %~dp0\Temp\') DO ( if %%a == %%f EXIT ) 
	start %~dp0\ScepSingleReinstall.CMD %%a
)

REM |Breakdown of commands above:
	REM |For /F => Accepts a file for processing
	REM |"skip=1 delims=" => What delimiters or seperator is to be used. Default is a space or tab. Also skips the first line in the file
	REM |If you are implementing this, make sure to modify the above section to match what Report.csv is using
	REM |%%a => Temporary variable to use for each item it breaks off
	REM |IN (%~dp0\Path\Filename.ext) => in the file called filename.ext in the folder Path in the same directory as this batch file
	REM |FOR /F %%f IN ('dir /b %~dp0\Temp\') DO ( if %1 == %%f EXIT ) => checks if the computer name from Report.csv is in the the temp folder meaning it has run on it successfully before
	REM |start %~dp0\ScepSingleReinstall.CMD %%a => for every item, start the batch file and pass it the item using %%a
	REM |Report title is Workstations - SCEP At Risk or Not Installed - List
	
	REM |https://ss64.com/nt/for.html 

