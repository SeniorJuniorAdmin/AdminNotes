# Get all the computer names in report.csv that aren't Name0 and aren't located in the temp folder 
foreach($line in Get-Content .\Report.csv) {
    Get-ChildItem ".\Temp" | ForEach-Object {
        if($line -ne "Name0" -and $_.FullName -ne $line) { # This is the line to tweak based on how report.csv is generated
            
            # This is the name of the log file that will be written to
            $log = (".\Logs\" + $line + ".log") 
            Add-Content $log -value "-----------------------------"
            Add-Content $log -value "$(Get-Date)"
            Add-Content $log -Value ("Pinging " + $line)
            
            # Store the output from pinging the computer name
            $pingOutput = (ping -n 1 $line) | Out-String 
            Add-Content $log -value $pingOutput 
            if($pingOutput -match "Received") {
                
                # If the ping went through successfully, uninstall SCEP 
                Add-Content $log -value "Computer Found, Uninstalling SCEP"
                Add-Content $log -Value "$(Invoke-Command -Computer '$line' -ScriptBlock {\\SERVERLOCATION\scepinstall.exe /u /s /q})"
                Add-Content $log -value "$(Get-Date)"

                # Install SCEP after uninstalling
                Add-Content $log -value "Finished Uninstalling, Installing SCEP"
                Add-Content $log -value "$(Invoke-Command -Computer '$line' -ScriptBlock {\\SERVERLOCATION\scepinstall.exe /s /q})"
                Add-Content $log -value "$(Get-Date)"
                
                # Mark in the log when the installation finished
                Add-Content $log -value "Finished Installing, marking as completed"
                Add-Content .\Temp\$line -value "Blank"
            }
            
            # Otherwise the ping failed to reach the computer so log it to the log and quit
            else {
                Add-Content $log -value "Computer unreachable, Aborting"
            }
        }
    }
}