@echo off
@setlocal enabledelayedexpansion
REM |This is the start of the log, If there is already a log for this computer it will just append to that log file
REM |Logs are stored in a folder called Logs inside the SCEPAutomation folder
REM |%~dp0 Means the path becomes drive letter (d)\path to this batch file (p)\Logs\
REM |%1 stands for the first argument passed to this file, in this case the computer name we are working on

REM |Add basic information to the log
echo ---------->> %~dp0\Logs\%1.log
echo Start Time and Date: >> %~dp0\Logs\%1.log
Date /T >> %~dp0\Logs\%1.log
Time /T >> %~dp0\Logs\%1.log

REM |Store a single ping to the computer name in the log
echo Pinging %1: >> %~dp0\Logs\%1.log
ping -n 1 %1 >> %~dp0\Logs\%1.log

REM |Check if a ping contains received indicating that the computer is online or unreachable indicating that it is offline
FOR /F "tokens=5,6,7" %%a IN ('ping -n 1 %1') DO 
(
	if "x%%b"=="xunreachable." goto :disconnect
    if "x%%a"=="xReceived" if "x%%c"=="x1,"  goto :connect
)

:disconnect 
	REM |If the computer is not online, don't do anything and close the command prompt
	echo No connection made... >> %~dp0\Logs\%1.log
	echo No changes made, Closing down >> %~dp0\Logs\%1.log
	EXIT

:connect

	echo Uninstalling SCEP on %1 >> %~dp0\Logs\%1.log

	REM |This command remotely uninstalls SCEP(/u), silently (/s), and quietly (/q)
	psexec \\%1 \\SERVERLOCATION\scepinstall.exe /u /s /q 2>> %~dp0\Logs\%1.log
	echo Finished uninstalling SCEP on %1 >> %~dp0\Logs\%1.log
	time /T >> %~dp0\Logs\%1.log
	
	echo Installing SCEP on %1 >> %~dp0\Logs\%1.log

	REM |This command remotely installs SCEP, silently (/s), and quietly (/q)
	psexec \\%1 \\SERVERLOCATION\scepinstall.exe /s /q 2>> %~dp0\Logs\%1.log
	echo Finished installing SCEP on %1 at: >> %~dp0\Logs\%1.log
	time /T >> %~dp0\Logs\%1.log

	REM |If the script completed, mark it as completed by making a file in the \Temp folder
	echo - >> %~dp0\Temp\%1
	EXIT
