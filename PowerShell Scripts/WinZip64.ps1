# Start the installer, for a manula installation
& 'msiexec.exe' /i"\\SERVERLOCATION\WINZIP180-64.MSI"

# Wait for the installation to finish
Wait-Process -Name "msiexec" 

# Copy the registration file
cp "\\SERVERLOCATION\WinZip18.wzmul" "C:\ProgramData\WinZip\WinZip.wzmul"
