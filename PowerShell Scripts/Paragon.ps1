# This file is for automatically creating a bootable USB drive for paragon

# Using this script:
# Change the DEFAULT LOCATION to whatever your default location is
# Start the script
# Answer the questions

function convertDiskNumbertoLetter 
{
    Param($DiskNumber = -1)

    # These 2 lines create a diskpart command
    New-Item -Name listDisk.txt -ItemType file -Force | Out-Null
    Add-Content -Path listDisk.txt "List Disk"

    # Get the output from running the Diskpart command
    $ListDisk = (diskpart /s listDisk.txt)
    Remove-Item -Path listDisk.txt

    $driveLetter = ""

    # If disk doesn't exist
    if ([int]$DiskNumber -lt 0 -or [int]$DiskNumber -gt [int]$ListDisk.count)
    {
        Write-Host "Please Enter a valid Disk Number with the -DiskNumber Parameter"
    }
    else 
    {
        # Get the details of the disk
        New-Item -Name detailDisk.txt -ItemType file -Force | Out-Null
        Add-Content -Path detailDisk.txt ("select disk " + $DiskNumber)
        Add-Content -Path detailDisk.txt "detail disk"
        $disks = (diskpart /s detailDisk.txt)
        $disksLastLine = $disks[$disks.count-1]
        Remove-Item -Path detailDisk.txt

        # Match the drive letters from WMI to the disk details
        Get-WmiObject -Class Win32_Volume | ForEach-Object{ 
            if (!([string]::IsNullOrEmpty($_.DriveLetter)) -and $disksLastLine -like ("* " + $_.DriveLetter.SubString(0,1) + " *"))
            {
                 $driveLetter = $_.DriveLetter
            }
        }
    }

    # Return the found drive letter
    $driveLetter
}

# Start Program
Write-Host
Write-Host "This script will create a bootable USB drive."
Write-Host "Make sure the USB is plugged in before continuing"
Write-Host
Read-Host -Prompt "Press Enter when ready to continue"


# These 2 lines create a diskpart command
New-Item -Name listDisk.txt -ItemType file -Force | Out-Null
Add-Content -Path listDisk.txt "List Disk"

# Get the output from running the diskpart command
$ListDisk = (diskpart /s listDisk.txt)
Remove-Item -Path listDisk.txt

# Output the disk number - status - size - free - Dyn - Gpt
for($line = 5; $line -le ($ListDisk.count-1); $line++){ Write-Host $ListDisk[$line] }

# Get which disk the user wants to convert
Write-Host 
Write-Host "If your disk is not present, enter -1"
$DiskNumber = Read-Host -Prompt "Input the Disk Number"
Write-Host


# Make sure the input is valid
while (!([int]$DiskNumber -ge -1 -and [int]$DiskNumber -le [int]($ListDisk.count-8))))
{ $DiskNumber = Read-Host -Prompt "That is not a valid input, please enter the disk number" } 

if ($DiskNumber -eq -1){ break }

# Confirm that the user want to delete all files on USB Drive
Write-Host "This drive letter is " (convertDiskNumbertoLetter -DiskNumber $DiskNumber)
$confirm = Read-Host -Prompt "This will delete all files on this drive, are you sure? (y/n) "

if($confirm -eq "y")
{
    # Add the neccesary commands to make the bootable disk ot the file
    New-Item -Name createDisk.txt -ItemType file -Force | Out-Null
    Add-Content -Path createDisk.txt ("Select disk " + $DiskNumber)
    Add-Content -Path createDisk.txt "clean"
    Add-Content -Path createDisk.txt "create partition primary"
    Add-Content -Path createDisk.txt "select partition 1"
    Add-Content -Path createDisk.txt "active"
    Add-Content -Path createDisk.txt "format fs=ntfs quick"

    $output = (diskpart /s createDisk.txt)
    Remove-Item -Path createDisk.txt

    # Output the result
    Write-Host 
    Write-Host $output[$output.count-1]

    # Copy the files needed to USB drive
    Write-Host
    $location = "SERVERLOCATION" # CHANGE THIS LINE TO YOUR SERVERLOCATION
    Write-Host ("Default Location is " + $location)
    $confirm = Read-Host -Prompt "Use the default file location? (y/n)"

    # If user want to input the non default location
    if($confirm -neq "y")
    {
        # Use a user specified path
        $location = Read-Host -Prompt "Please enter the location of the files to copy to the drive"
    }

    xcopy $location ((convertDiskNumbertoLetter -DiskNumber $DiskNumber) + "\") /S /E /Q

    # Ask the user if they want to eject the disk
    Write-Host 
    $confirm = Read-Host -Prompt "Do you want to eject the disk? (y/n)"

    if($confirm -eq "y")
    {
        # Eject the drive
        $driveLetter = (convertDiskNumbertoLetter -DiskNumber $DiskNumber)
        $driveEject = New-Object -comObject Shell.Application
        $driveEject.Namespace(17).ParseName($driveLetter).InvokeVerb("Eject")
    }
}


