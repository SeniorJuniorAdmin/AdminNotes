﻿#Create a Log File
$log = ".\AdobeCaptivateInstallation.log"
Add-content $log -Value "Start Time: $(Get-Date)" #Log start time

#Start the installation
$setup = (Start-Process -FilePath "\\msp00-cmps01\Software\Adobe\Adobe Captivate\AdobeCaptivate-Win64\Build\setup.exe")

#Logging values
Add-content $log -Value "$(Get-Date) - Installation Started"
Add-content $log -Value "Setup Process Information:"

Add-Content $log -Value "---------------------------------------"
$setupProcess = Get-Process -Name "explorer" -ErrorAction SilentlyContinue
$setupProcess | ForEach-Object{ Add-content $log -Value ("Setup Process ID:" + $_.Id + " | Exited:" + $_.HasExited + " | Start Time: " + $_.StartTime) }
Add-Content $log -Value "---------------------------------------`n"

Add-content $log -Value "Explorer Process Information:"
Add-Content $log -Value "---------------------------------------"
$process = Get-Process -Name "explorer" -ErrorAction SilentlyContinue
$process | ForEach-Object{ Add-content $log -Value ("Explorer Process ID:" + $_.Id + " | Exited:" + $_.HasExited + " | Start Time: " + $_.StartTime) }
Add-Content $log -Value "---------------------------------------`n"

$explorer = ""

#Wait for explorer to stop
#Check all processes found by Get-Process to see if any stop
#Also check if the installation is still running
while(Get-Process -Name "setup" -ErrorAction SilentlyContinue)
{
    $process | ForEach-Object{
        if(!(Get-Process -Id $_.Id -ErrorAction SilentlyContinue)){
            Add-Content $log -Value ("Explorer Process Stopped: " + $_.HasExited + " | ID: " + $_.Id)
            $explorer = $_
            break
        }
        else 
        {
            Add-Content $log -Value ("Explorer Process Still Running: " + $(Get-Date) + " | ID: " + $_.Id)
        }
    }
    Start-Sleep -Seconds 15
}

# If the installation finished without closing explorer
if(!(Get-Process -Name "setup" -ErrorAction SilentlyContinue))
{
    Start-Sleep -Seconds 15 
    Add-content $log -Value "$(Get-Date) - Installation Closed"
    Add-content $log -Value "No Explorer Process stopped running"
    exit
}

#Logging values
Add-content $log -Value "$(Get-Date) - Explorer Closed"
Add-Content $log -Value ("Other Explorer Process IDs: " + (Get-Process -Name "explorer" -ErrorAction SilentlyContinue).Id)

#Start explorer.exe processes until one actually starts
while (!(Get-Process -Id $explorer.Id -ErrorAction SilentlyContinue) -or $explorer.Id -eq $null) 
{ 
    $explorer = (Start-Process explorer.exe) 
    Add-content $log -Value "$(Get-Date) - Explorer Restart Attempt"
    Add-Content $log -Value ("Explorer Process Name: " + $explorer.Name + ", ID: " + $explorer.Id)
}

#Logging values
Add-content $log -Value "$(Get-Date) - Explorer Restarted"
Add-Content $log -Value ("Explorer Process Name: " + $explorer.Name + ", ID: " + $explorer.Id)
Add-Content $log -Value ("Get-Process ID: " + (Get-Process -Name "explorer" -ErrorAction SilentlyContinue).Id)

#Wait for the installation to finish before closing
While (Get-Process -Name "setup" -ErrorAction SilentlyContinue) 
{ 
    Start-Sleep -Seconds 15
    #Logging values
    Add-content $log -Value "$(Get-Date) - Installation Still Present" 
    Add-Content $log -Value ("Get-Process ID: " + (Get-Process -Name "setup" -ErrorAction SilentlyContinue).Id)
}

#Logging Values
Add-content $log -Value "$(Get-Date) - Setup Closed"
Add-Content $log -Value ("Get-Process ID: " + (Get-Process -Name "setup" -ErrorAction SilentlyContinue).Id)

# Wait for a 15 seconds before letting SCCM check for software
Start-Sleep -Seconds 15 
Add-content $log -Value "$(Get-Date) - Installation Closed"
